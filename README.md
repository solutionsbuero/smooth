# Smooth – A simple pandoc wrapper
Smooth allows to set all necessary options for Pandoc within the source. Currently it only supports the conversion to PDFs via LaTeX. Because of the modular architecture (provided by the [Cement](http://builtoncement.com/)) framework, it's simple to add others.

## Requirements
- [Pandoc](https://pandoc.org/)
- [GPP (general-purpose preprocessor)](https://logological.org/gpp)
- LaTeX

## Usage
All options are set in YAML-preamble of the markdown document. For example:

```
---
  subject: A subject
  title: Just a title
  author: Solutionsbüro
  template: ../../../Templates/document.tex
---

# Introduction
Some Text...
```

### Latex
Currently Smooth allows only PDF output via LaTeX. Therefore the `latex` module is the only one at the moment.

```
smooth latex path_to_file.md
```

## Options
### General
| Option           | Description                   | Default      |
| ---------------- | ----------------------------- | ------------ |
| `template`       | Absolut path tho the template | ~/pandoc.tex |
| `gpp_enabled`    | Enables the GPP               | False        |
| `gpp_options`    | Set GPP Options               |              |
| `pandoc_options` | Set pandoc options            |              |

### LaTeX
Most options are depending on the LaTeX-Template, therefore this table reflects our situation.

| Option           | Description                                                                 | Default  | General            |
| ---------------- | --------------------------------------------------------------------------- | -------- | ------------------ |
| `engine`         | Specifies the LaTeX-engine to use.                                          | xelatex  | :heavy_check_mark: |
| `numbersections` | When true, the sections are numbered.                                       | False    | :x:                |
| `subject`        | Subject of the document.                                                    |          | :x:                |
| `title`          | Title of the document. When no title is set, there will be no `\maketitle`. |          | :x:                |
| `subtitle`       | Subtitle of the document.                                                   |          | :x:                |
| `author`         | Author of the document.                                                     |          | :x:                |
| `date`           | Date of the document.                                                       | `\today` | :x:                |
| `publishers`     | Publishers of the document.                                                 |          | :x:                |
| `titlepage`      | Shows a titlepage. Otherwise a custom header is shown.                      | False    | :x:                |


## License
Smooth is licensed under the GNU GENERAL PUBLIC LICENSE Version 3.