from cement.core.foundation import CementApp
from controllers.latex import LatexController
from controllers.base import BaseController


class Application(CementApp):
    class Meta:
        label = 'smooth'
        base_controller = 'base'
        handlers = [BaseController, LatexController]
