#!/usr/bin/env python3

from application import Application


with Application() as app:
    app.run()
