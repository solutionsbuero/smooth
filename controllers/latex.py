from cement.core.controller import expose
from controllers.abstract import AbstractController
import subprocess
import os


class LatexController(AbstractController):
    class Meta:
        label = 'latex'
        stacked_on = 'base'
        stacked_type = 'embedded'
        description = "LaTeX export"

    @expose(help="LaTeX export.", hide=False)
    def latex(self):
        self.initialize()
        self.convert(self.document)

    @staticmethod
    def convert(document):
        output_file = os.path.basename(document.path).split('.', 1)[0] + ".pdf"

        if document.gpp_enabled and document.gpp_options == "":
            gpp_call = ["gpp", document.path]
        elif document.gpp_enabled:
            gpp_call = ["gpp", document.gpp_options, document.path]
        else:
            gpp_call = ["cat", document.path]
        preprocessed = subprocess.Popen(gpp_call, stdout=subprocess.PIPE)

        if document.pandoc_options == "":
            pandoc_call = ["pandoc", "--template", document.template, "--latex-engine", document.engine, "-o",
                           output_file]
        else:
            pandoc_call = ["pandoc", "--template", document.template, "--latex-engine", document.engine,
                           document.pandoc_options, "-o", output_file]
        subprocess.Popen(pandoc_call, stdin=preprocessed.stdout)
