from cement.core.controller import CementBaseController
import subprocess
import json
import os


class Document(object):
    path = ""
    template = ""
    engine = ""
    gpp_enabled = False
    gpp_options = ""
    pandoc_options = ""

    def __init__(self, path):
        self.path = path


class AbstractController(CementBaseController):
    document = Document
    metadata = json

    def initialize(self):
        self.document = Document(self.app.pargs.input_file[0])
        self.metadata = self.extract_json(self.document)
        self.extract_metadata(self.metadata)

    @staticmethod
    def extract_document_path(args):
        if len(args) != 2:
            print("Invalid argument(s).\n")
            quit()
        return args[1]

    @staticmethod
    def extract_json(document):
        metadata_template = open("/tmp/smooth_metadata_template.pandoc-tpl", "w")
        metadata_template.write("$meta-json$")
        metadata_template.close()
        raw_json = subprocess.check_output(["pandoc", "--template", metadata_template.name, document.path])
        os.remove(metadata_template.name)
        return json.loads(raw_json.decode("utf-8"))

    def extract_metadata(self, metadata):
        if "template" in metadata:
            self.document.template = metadata["template"]
        else:
            self.document.template = "~/pandoc.tex"

        if "engine" in metadata:
            self.document.engine = metadata["engine"]
        else:
            self.document.engine = "xelatex"

        if "gpp_enabled" in metadata:
            self.document.gpp_enabled = True

        if "gpp_options" in metadata:
            self.document.gpp_options = metadata["gpp_options"]

        if "pandoc_options" in metadata:
            self.document.pandoc_options = metadata["pandoc_options"]