from cement.core.controller import expose
from controllers.abstract import AbstractController


class BaseController(AbstractController):

    class Meta:
        label = 'base'
        description = "Simple wrapper for pandoc."
        arguments = [
            (['input_file'],
             dict(action='store', nargs='*'))
        ]

    @expose(help="Default information output.", hide=True)
    def default(self):
        print("Call \"smooth -h\" for usage information.")
